# simple-shopping-cart-service


##Instructions 

01. Create a database in postgres.
        
02. Create a new schema called 'product_schema'.
        
03. Execute the following samples queries for creating a table and adding sample data.
            
            CREATE TABLE products (
                id BIGSERIAL PRIMARY KEY,
                productcode VARCHAR(100) DEFAULT NULL,
                productname VARCHAR(100) DEFAULT NULL,
                productdescription VARCHAR(250) NULL,
                curency VARCHAR(50) DEFAULT NULL,
                unitspercarton INT DEFAULT NULL,
                singlecartonprice FLOAT DEFAULT NULL,
                margineforsingleunit FLOAT DEFAULT NULL,
                minnumofcartons INT DEFAULT NULL,
                discount FLOAT DEFAULT NULL,
                singleunitprice FLOAT DEFAULT NULL
            );

            INSERT INTO product_schema.products (productcode,productname,productdescription,curency,unitspercarton,singlecartonprice,   margineforsingleunit,minnumofcartons,discount,singleunitprice) VALUES ( 'P001', 'Penguin-ears', 'The rare product Penguin-ears', 'USD','20','175','30','3','10','11.375');

            INSERT INTO product_schema.products (productcode,productname,productdescription,curency,unitspercarton,singlecartonprice,margineforsingleunit,minnumofcartons,discount,singleunitprice) VALUES ('P002', 'Horseshoe', 'The product Horseshoe', 'USD','5','825','30','3','10','214.5');

04. Change the data source name in application.properties.

05. Build reactjs application and then copy build files to webapps folder.
        
        React application URL 
            URL :- https://gitlab.com/hansanaherath/simple-shopping-cart-web.git
        
        Install dependencies and build
            
            npm install
            
            npm run build
        
        copy files
            
            cp -R build/* /simple-shopping-cart-service/src/main/webapp/

03. Build and deploy Java application.

04. To setup a new product, use folowing URL
        URL :- http://localhost:8282/setupNewProduct
    
05. To buy a product, use following URL
        URL :- http://localhost:8282/shoppingCart

