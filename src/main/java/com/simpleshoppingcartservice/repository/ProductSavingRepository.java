package com.simpleshoppingcartservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simpleshoppingcartservice.entity.SingleProduct;

public interface ProductSavingRepository extends JpaRepository<SingleProduct, Long> {

	SingleProduct findAllById(long id);

}
