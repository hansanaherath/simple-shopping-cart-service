package com.simpleshoppingcartservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simpleshoppingcartservice.entity.SingleProduct;
import com.simpleshoppingcartservice.repository.ProductSavingRepository;

@Service
public class CartOperationsServiceImpl implements CartOperationService {

	@Autowired
	private ProductSavingRepository productSavingRepository;

	@Transactional
	@Override
	public List<SingleProduct> fetchProducts() throws Exception {
		return productSavingRepository.findAll();
	}

	@Transactional
	@Override
	public String fetchTotalPrice(Integer itemCount, Integer id) throws Exception {
		SingleProduct product = productSavingRepository.findAllById((long) id);
		CommonUtil util = new CommonUtil();

		int numOfCartonsShouldBuyDiscount = product.getMinNumOfCartons();
		int unitsPerCarton = product.getUnitsPerCarton();
		int orderedCartonCount = itemCount / unitsPerCarton;
		int remainingUnitsCount = itemCount % unitsPerCarton;
		double singleCartonPrice = product.getSingleCartonPrice();
		double singleUnitPrice = product.getSingleUnitPrice();
		double priceForAllCartons = 0.0;
		double priceForRemainingUnits = 0.0;
		double grandTotal = 0.0;
		double discountValue = product.getDiscount();

		if (numOfCartonsShouldBuyDiscount <= orderedCartonCount) {
			priceForAllCartons = orderedCartonCount * singleCartonPrice * ((100.0 - discountValue) / 100.0);
			priceForRemainingUnits = remainingUnitsCount * singleUnitPrice;
			grandTotal = priceForAllCartons + priceForRemainingUnits;
		} else {
			priceForAllCartons = orderedCartonCount * singleCartonPrice;
			priceForRemainingUnits = remainingUnitsCount * singleUnitPrice;
			grandTotal = priceForAllCartons + priceForRemainingUnits;

		}

		Double returnedPrice = util.round(grandTotal, 3);

		return returnedPrice.toString();
	}
}
