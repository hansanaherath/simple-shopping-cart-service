package com.simpleshoppingcartservice.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simpleshoppingcartservice.entity.SingleProduct;
import com.simpleshoppingcartservice.repository.ProductSavingRepository;

@Service
public class ProductsImpl implements Products {

	@Autowired
	private ProductSavingRepository productSavingRepository;

	@Override
	public void savingProducts(LinkedList<SingleProduct> productList) throws Exception {
		CommonUtil util = new CommonUtil();

		for (SingleProduct singleProduct : productList) {
			double margine = singleProduct.getMargineForSingleUnit();
			int unitsPerCorton = singleProduct.getUnitsPerCarton();
			double cartonPrice = singleProduct.getSingleCartonPrice();

			double singleUnitPrice = (cartonPrice * ((100.0 + margine) / 100.0)) / unitsPerCorton;

			singleProduct.setSingleUnitPrice(util.round(singleUnitPrice, 3));

		}

		productSavingRepository.saveAll(productList);
		productSavingRepository.flush();

	}

	@Transactional
	@Override
	public List<SingleProduct> fetchAllProducts() throws Exception {
		return productSavingRepository.findAll();
	}

	@Override
	public void deleteProductList(LinkedList<SingleProduct> productList) {
		productSavingRepository.deleteInBatch(productList);

	}
}
