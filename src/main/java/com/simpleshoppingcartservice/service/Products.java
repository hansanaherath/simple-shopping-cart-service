package com.simpleshoppingcartservice.service;

import java.util.LinkedList;
import java.util.List;

import com.simpleshoppingcartservice.entity.SingleProduct;

public interface Products {

	void savingProducts(LinkedList<SingleProduct> productList) throws Exception;

	List<SingleProduct> fetchAllProducts() throws Exception;

	void deleteProductList(LinkedList<SingleProduct> productList);

}
