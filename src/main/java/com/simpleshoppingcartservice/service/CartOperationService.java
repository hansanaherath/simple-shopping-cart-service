package com.simpleshoppingcartservice.service;

import java.util.List;

import com.simpleshoppingcartservice.entity.SingleProduct;

public interface CartOperationService {

	List<SingleProduct> fetchProducts() throws Exception;

	String fetchTotalPrice(Integer itemCount, Integer id) throws Exception;

}
