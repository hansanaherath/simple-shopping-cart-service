package com.simpleshoppingcartservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products", schema = "product_schema")
public class SingleProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, columnDefinition = "serial")
	private Long id;

	@Column(name = "productcode")
	private String productCode;

	@Column(name = "productname")
	private String productName;

	@Column(name = "productdescription")
	private String productDescription;

	@Column(name = "curency")
	private String curency;

	@Column(name = "unitspercarton")
	private Integer unitsPerCarton;

	@Column(name = "singlecartonprice")
	private Double singleCartonPrice;

	@Column(name = "margineforsingleunit")
	private Double margineForSingleUnit;

	@Column(name = "minnumofcartons")
	private Integer minNumOfCartons;

	@Column(name = "discount")
	private Double discount;

	@Column(name = "singleunitprice")
	private Double singleUnitPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getUnitsPerCarton() {
		return unitsPerCarton;
	}

	public void setUnitsPerCarton(Integer unitsPerCarton) {
		this.unitsPerCarton = unitsPerCarton;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getCurency() {
		return curency;
	}

	public void setCurency(String curency) {
		this.curency = curency;
	}

	public Double getSingleCartonPrice() {
		return singleCartonPrice;
	}

	public void setSingleCartonPrice(Double singleCartonPrice) {
		this.singleCartonPrice = singleCartonPrice;
	}

	public Double getMargineForSingleUnit() {
		return margineForSingleUnit;
	}

	public void setMargineForSingleUnit(Double margineForSingleUnit) {
		this.margineForSingleUnit = margineForSingleUnit;
	}

	public Integer getMinNumOfCartons() {
		return minNumOfCartons;
	}

	public void setMinNumOfCartons(Integer minNumOfCartons) {
		this.minNumOfCartons = minNumOfCartons;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getSingleUnitPrice() {
		return singleUnitPrice;
	}

	public void setSingleUnitPrice(Double singleUnitPrice) {
		this.singleUnitPrice = singleUnitPrice;
	}

}
