package com.simpleshoppingcartservice.models;

public class ResponseProductObject {

	private Long id;
	private String productCode;
	private String productName;
	private String curency;
	private Integer numOfCartons;
	private Integer unitsPerCarton;
	private Double singleCartonPrice;
	private Double margineForSingleUnit;
	private Integer minNumOfCartons;
	private Double discount;
	private Double singleUnitPrice;

	public ResponseProductObject(Long id, String productCode, String productName, String curency, Integer numOfCartons,
			Integer unitsPerCarton, Double singleCartonPrice, Double margineForSingleUnit, Integer minNumOfCartons,
			Double discount, Double singleUnitPrice) {
		this.id = id;
		this.productCode = productCode;
		this.productName = productName;
		this.curency = curency;
		this.numOfCartons = numOfCartons;
		this.unitsPerCarton = unitsPerCarton;
		this.singleCartonPrice = singleCartonPrice;
		this.margineForSingleUnit = margineForSingleUnit;
		this.minNumOfCartons = minNumOfCartons;
		this.discount = discount;
		this.singleUnitPrice = singleUnitPrice;
	}
	
	public ResponseProductObject() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCurency() {
		return curency;
	}

	public void setCurency(String curency) {
		this.curency = curency;
	}

	public Integer getNumOfCartons() {
		return numOfCartons;
	}

	public void setNumOfCartons(Integer numOfCartons) {
		this.numOfCartons = numOfCartons;
	}

	public Integer getUnitsPerCarton() {
		return unitsPerCarton;
	}

	public void setUnitsPerCarton(Integer unitsPerCarton) {
		this.unitsPerCarton = unitsPerCarton;
	}

	public Double getSingleCartonPrice() {
		return singleCartonPrice;
	}

	public void setSingleCartonPrice(Double singleCartonPrice) {
		this.singleCartonPrice = singleCartonPrice;
	}

	public Double getMargineForSingleUnit() {
		return margineForSingleUnit;
	}

	public void setMargineForSingleUnit(Double margineForSingleUnit) {
		this.margineForSingleUnit = margineForSingleUnit;
	}

	public Integer getMinNumOfCartons() {
		return minNumOfCartons;
	}

	public void setMinNumOfCartons(Integer minNumOfCartons) {
		this.minNumOfCartons = minNumOfCartons;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getSingleUnitPrice() {
		return singleUnitPrice;
	}

	public void setSingleUnitPrice(Double singleUnitPrice) {
		this.singleUnitPrice = singleUnitPrice;
	}

}
