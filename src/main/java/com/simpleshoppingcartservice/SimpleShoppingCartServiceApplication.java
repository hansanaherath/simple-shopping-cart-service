package com.simpleshoppingcartservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleShoppingCartServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleShoppingCartServiceApplication.class, args);
	}

}
