package com.simpleshoppingcartservice.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemplateControllers {

	@GetMapping("/shoppingCart")
	public String getLoginView() {
		return "shoppingCart.html";
	}
	
	@GetMapping("/setupNewProduct")
	public String getHomePage() {
		return "newProduct.html";
	}
}
