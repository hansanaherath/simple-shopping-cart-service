package com.simpleshoppingcartservice.controllers;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.simpleshoppingcartservice.entity.SingleProduct;
import com.simpleshoppingcartservice.service.CartOperationService;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("setups/cartOperation")
public class CartOperationController {

	@Autowired
	private CartOperationService cartOperationService;

	@GetMapping("/fetchProducts")
	public ResponseEntity<List<SingleProduct>> fetchAllProducts() {
		List<SingleProduct> productList = Collections.emptyList();
		try {
			productList = cartOperationService.fetchProducts();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!productList.isEmpty()) {
			return new ResponseEntity<>(productList, HttpStatus.OK);
		}

		return new ResponseEntity<>(productList, HttpStatus.NO_CONTENT);
	}

	@GetMapping("/getTotalPrice")
	public ResponseEntity<String> fetchTotalPrice(@RequestParam(value="itemCount", required=true) Integer itemCount,@RequestParam(value="id", required=true) Integer id) {
		String price = null;
		try {
			price = cartOperationService.fetchTotalPrice(itemCount,id);
			return new ResponseEntity<>(price, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(price, HttpStatus.NO_CONTENT);
	}
}
