package com.simpleshoppingcartservice.controllers;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simpleshoppingcartservice.entity.SingleProduct;
import com.simpleshoppingcartservice.service.Products;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/setups/products")
public class SetupProductController {

	@Autowired
	private Products products;

	@CrossOrigin(origins = "*")
	@PostMapping("/saveProducts")
	public ResponseEntity<String> saveProducts(@RequestBody LinkedList<SingleProduct> productList) {
		if (productList.isEmpty()) {
			return new ResponseEntity<String>("Nothing To Save", HttpStatus.PRECONDITION_FAILED);
		}
		try {
			products.savingProducts(productList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>("Data saved successfull", HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/fetchProductsList")
	public ResponseEntity<List<SingleProduct>> fetchAllProducts() {
		List<SingleProduct> productList = Collections.emptyList();
		try {
			productList = products.fetchAllProducts();

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!productList.isEmpty()) {
			return new ResponseEntity<>(productList, HttpStatus.OK);
		}

		return new ResponseEntity<>(productList, HttpStatus.NO_CONTENT);
	}

	@CrossOrigin(origins = "*")
	@DeleteMapping("/deleteProductList")
	public ResponseEntity<String> deleteProducts(@RequestBody LinkedList<SingleProduct> productList) {
		if (productList.isEmpty()) {
			return new ResponseEntity<>("Nothing To Delete", HttpStatus.PRECONDITION_FAILED);
		}
		try {
			products.deleteProductList(productList);
			return new ResponseEntity<>("Data Deleted successfull", HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>("Some Things Went Wrongs", HttpStatus.NO_CONTENT);
	}

}
